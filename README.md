# hadoop-playbooks

_hadoop-playbooks_ is a collection of Ansible playbooks for deployment and basic management of Hadoop
and its various components on a Debian 10 server cluster.

It uses [Apache Bigtop's](https://bigtop.apache.org/)
[1.5.0](https://cwiki.apache.org/confluence/display/BIGTOP/Bigtop+1.5.0+Release) release to obtain
package repository for most of Hadoop components.

## Usage
Run playbooks from `playbooks` directory like this:
```
ansible-playbook -i hosts -u <USER> -K playbooks/<PLAYBOOK>.yml
```

To deploy all Hadoop services at once, run `playbooks/deploy_cluster.yml` playbook.

Be aware that some playbooks may ask you whether you're performing first time installation. Answer
*yes* **ONLY** when actually performing first time installation on a clean cluster! For first time
installation there are some commands, e.g. formatting HDFS or initializing Hive Metastore schemas,
that might lose or corrupt existing data if performed on already existing Hadoop installation.

### Inventory file
To run these playbooks, user needs to provide an inventory file with a list of servers split into
specific groups used by the playbooks.

An example inventory file is provided in `hosts_example`. The 5 groups in this example file need
to be specified in any inventory file given to these playbooks for them to work properly.

### Group variables
`group_vars/all/main.yml` contains a list of variables used in these playbooks with some prefilled
values. It is *strongly* advised for any user of these playbooks to look through all these variables
and modify them if necessary to fit given use case. Especially `data_mounts` directories and Trino's
memory limits should be modified to fit user's storage and memory conditions.

### Security
These playbooks do NOT provide any configuration for securing access to the cluster using Kerberos
or any other mechanism. If you want to secure your Hadoop cluster, see documentation for each given
Hadoop component on how to make the necessary configuration changes.

## Scripts
There are two scripts in `scripts` directory -- `manage_component.py` and `manage_service.py`. These
scripts are wrappers around simple ansible commands to manage *systemd/init.d* services or entire
Hadoop components on the cluster.

*manage_service.py* - Performs *start/stop/restart/enable/disable/status* action for a given service
on servers supplied by inventory.

*manage_component.py* - Performs *start/stop/restart/enable/disable/status* action for an entire
Hadoop component on servers supplied by inventory file. It performs one of these actions on all
*systemd/init.d* services associated with given Hadoop component. E.g. for YARN the script performs
given action on the following services: *hadoop-yarn-nodemanager*, *hadoop-yarn-resourcemanager*,
*hadoop-mapreduce-historyserver*.

## Useful links
After running these playbooks some of the deployed services will provide a web interface overview:

  - HDFS overview: http://{primary-control-node}:50070/, http://{secondary-control-node}:50070/
  - YARN Resource Manager: http://{primary-control-node}:8088/, http://{secondary-control-node}:8088/
  - Hive Server2: http://{primary-control-node}:10002/, http://{secondary-control-node}:10002/
  - Trino Catalog: http://{primary-control-node}:8080/
  - Spark History Server: http://{primary-control-node}:18080/

## Distribution of Hadoop services on the cluster
Hadoop cluster deployed by these playbooks consists of the following services:
[Hadoop (HDFS + MapReduce + YARN)](https://hadoop.apache.org/), [Zookeeper](https://zookeeper.apache.org/),
[Spark](https://spark.apache.org/), [Hive](https://hive.apache.org/) and [Trino](https://trino.io/).
In the diagram below you can see how these services are distributed on the cluster.

![Hadoop services on the cluster](hadoop_cluster_services.png)

## Playbooks overview

### `playbooks/setup_env.yml`
This playbook installs packages for some prerequisites and adds Debian 10 repositories for
Eclipse Temurin and Apache Bigtop 1.5.0 to the system.

### `playbooks/zookeeper.yml`
This playbook installs Zookeeper on all servers and Zookeeper Server on servers from
`zookeeper-servers` group in inventory file. Be aware that there should always be an odd number
of servers in `zookeeper-servers` group so they can form a quorum.

### `playbooks/hadoop.yml`
This playbook installs the main Hadoop component, HDFS + YARN + MapReduce, in a high-availability
setup with `primary-control-node` as primary HDFS NameNode and YARN ResourceManager and
`secondary-control-node` as standby HDFS NameNode and YARN ResourceManager.

It sets up the main Hadoop user and generates and distributes SSH keys from control nodes to data
nodes so servers in the cluster can communicate. It also creates directory structures for HDFS based
on `data_mounts` group variable. The playbook asks user if first time installation is happening and
if the answer is *yes* it formats and initializes a fresh HDFS filesystem.

### `playbooks/spark.yml`
This playbook installs Spark on all servers along with its Python and R interfaces. Spark History
Server is installed on primary control node. YARN is configured as Spark's backend.

### `playbooks/hive.yml`
This playbook installs Hive in a high-availability setup with `primary-control-node` and
`secondary-control-node` as Hive Metastores. It uses PostgreSQL 11 installed on `primary-control-node`
as a backend database for Hive Metastores which makes it not truly highly available as the PostgreSQL
database represents a possible single point of failure. It is left to the user to possibly rectify
this by configuring some mirroring/backup solution for the PostgreSQL database.

Before using this playbook user needs to setup PostgreSQL instance on primary control node and
configure it to be visible at \<primary-control-node\>:5432 to primary and secondary control nodes.

The playbook asks user if first time installation is happening and if the answer is *yes* it
creates and initializes the metastore database schema.

### `playbooks/trino.yml`
This playbook installs Trino SQL engine. It uses Hive Metastore to access data so previous
deployment of Hive is needed. `primary-control-node` is used as a Coordinator and `data-nodes` are
configured as Workers. `trino-shell` wrapper around Trino's CLI tool is installed as well as
a *systemd* service for both Coordinator and Workers.

It is *strongly* advised to properly setup Trino's memory limits in variables in
`group_vars/all/main.yml` according to your server's hardware or Trino may not work correctly.

### `playbooks/deploy_cluster.yml`
This playbook deploys all above mentioned playbooks at once in the correct order. Use this playbook
as a reference even if you want to deploy only a subset of the provided Hadoop components because
some of the playbooks assume previous deployment of other playbooks written above them in this
playbook, especially `setup_env.yml`, `zookeeper.yml` and `hadoop.yml`.
