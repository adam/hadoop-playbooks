#!/usr/bin/env python3

import os
import argparse

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-g', dest='HOST_GROUP', default='all', help='Inventory host group. Ignored with -i option')
    inv_group = parser.add_mutually_exclusive_group(required=True)
    inv_group.add_argument('-i', dest='INVENTORY', help='Comma-separated list of hosts with ending comma')
    inv_group.add_argument('-I', dest='INVENTORY_FILE', help='Inventory file')
    parser.add_argument('-b', '--become', action='store_true', help='Run operations with become')
    parser.add_argument('-K', action='store_true', help='Ask for privilege escalation password')
    parser.add_argument('-k', action='store_true', help='Ask for connection password')
    parser.add_argument('-u', '--user', dest='REMOTE_USER', help='Connect as this user')
    parser.add_argument('service', help='Name of specific Hadoop service (hadoop-hdfs-datanode)')
    parser.add_argument('action', choices=['start', 'stop', 'restart', 'enable', 'disable', 'status'], help='Action to perform for the service')
    args = parser.parse_args()

    if args.INVENTORY:
        args.HOST_GROUP = 'all'

    host_group = ' {}'.format(args.HOST_GROUP)
    inventory = ' -i {}'.format(args.INVENTORY if args.INVENTORY else args.INVENTORY_FILE)
    become = ' -b' if args.become else ''
    priv_paswd = ' -K' if args.K else ''
    conn_paswd = ' -k' if args.k else ''
    user = ' -u {}'.format(args.REMOTE_USER) if args.REMOTE_USER else ''

    cmd = 'ansible {}{}{}{}{}{} -m command -a "systemctl {} {}"'.format(
        host_group,
        inventory,
        become,
        priv_paswd,
        conn_paswd,
        user,
        args.action,
        args.service
    )

    print(cmd)
    os.system(cmd)

if __name__ == '__main__':
    main()
