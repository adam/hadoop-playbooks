#!/usr/bin/env python3

import os
import argparse

hadoop_components = ['hdfs', 'yarn', 'zookeeper', 'spark', 'hive', 'trino', 'all']
actions = ['start', 'stop', 'restart', 'status', 'enable', 'disable']

def run_cmd(args, nodes, service):
    cmd = 'ansible {} -i {} {} {} {} {} -m command -a "systemctl {} {}"'.format(
            nodes,
            args.INVENTORY_FILE,
            ' -b' if args.become else '',
            ' -K' if args.K else '',
            ' -k' if args.k else '',
            ' -u {}'.format(args.REMOTE_USER) if args.REMOTE_USER else '',
            args.action,
            service)
    os.system(cmd)

def run_procedure(args, ops):
    if args.action == 'stop':
        for op in reversed(ops):
            run_cmd(args, *op)
    else:
        for op in ops:
            run_cmd(args, *op)

def run_hdfs(args):
    '''
    hadoop-hdfs-datanode
    hadoop-hdfs-zkfc
    hadoop-hdfs-journalnode
    hadoop-hdfs-namenode
    '''

    operations = [
        ('control-nodes', 'hadoop-hdfs-namenode'),
        ('control-nodes', 'hadoop-hdfs-zkfc'),
        ('zookeeper-servers', 'hadoop-hdfs-journanode'),
        ('data-nodes', 'hadoop-hdfs-datanode')
    ]

    run_procedure(args, operations)

def run_yarn(args):
    '''
    hadoop-yarn-nodemanager
    hadoop-yarn-resourcemanager
    hadoop-mapreduce-historyserver
    '''

    operations = [
        ('control-nodes', 'hadoop-yarn-resourcemanager'),
        ('control-nodes', 'hadoop-mapreduce-historyserver'),
        ('data-nodes', 'hadoop-yarn-nodemanager')
    ]

    run_procedure(args, operations)

def run_zookeeper(args):
    '''
    zookeeper-server
    '''

    run_cmd(args, 'zookeeper-servers', 'zookeeper-server')

def run_spark(args):
    '''
    spark-history-server
    '''

    run_cmd(args, 'primary-control-node', 'spark-history-server')

def run_hive(args):
    '''
    hive-metastore
    hive-server2
    hive-hcatalog-server
    hive-webhcat-server
    '''

    operations = [
        ('control-nodes', 'hive-metastore'),
        ('control-nodes', 'hive-server2'),
        ('control-nodes', 'hive-hcatalog-server'),
        ('control-nodes', 'hive-webhcat-server')
    ]

    run_procedure(args, operations)

def run_trino(args):
    '''
    trino
    '''

    operations = [
        ('primary-control-node', 'trino'),
        ('data-nodes', 'trino')
    ]

    run_procedure(args, operations)

def run_all(args):
    if args.action == 'stop':
        run_trino(args)
        run_hive(args)
        run_spark(args)
        run_zookeeper(args)
        run_yarn(args)
        run_hdfs(args)
    else:
        run_hdfs(args)
        run_yarn(args)
        run_zookeeper(args)
        run_spark(args)
        run_hive(args)
        run_trino(args)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-I', dest='INVENTORY_FILE', help='Inventory file')
    parser.add_argument('-b', '--become', action='store_true', help='Run operations with become')
    parser.add_argument('-K', action='store_true', help='Ask for privilege escalation password')
    parser.add_argument('-k', action='store_true', help='Ask for connection password')
    parser.add_argument('-u', '--user', dest='REMOTE_USER', help='Connect as this user')
    parser.add_argument('component', choices=hadoop_components, help='Name of Hadoop component (Spark, Trino, ...)')
    parser.add_argument('action', choices=actions, help='Action to perform for the component')
    args = parser.parse_args()

    if args.component == 'hdfs':
        run_hdfs(args)
    elif args.component == 'yarn':
        run_yarn(args)
    elif args.component == 'zookeeper':
        run_zookeeper(args)
    elif args.component == 'spark':
        run_spark(args)
    elif args.component == 'hive':
        run_hive(args)
    elif args.component == 'trino':
        run_trino(args)
    elif args.component == 'all':
        run_all(args)

if __name__ == '__main__':
    main()
