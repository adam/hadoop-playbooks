SELECT 'CREATE USER {{ hive_db_user }}' WHERE NOT EXISTS (SELECT FROM pg_catalog.pg_roles WHERE rolname = 'hive')
\gexec
ALTER ROLE hive WITH PASSWORD '{{ hive_db_password }}';
SELECT 'CREATE DATABASE metastore' WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = 'metastore')
\gexec