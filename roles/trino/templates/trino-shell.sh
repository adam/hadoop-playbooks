#!/usr/bin/bash

/opt/trino/trino-shell --server {{ groups['primary-control-node'][0] }}:8080 --catalog hive "$@"
